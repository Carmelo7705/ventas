const tokenService = require("../services/token");

module.exports = {
  userVerify: async (req, res, next) => {
    let token = req.headers.token;

    if (!token) {
      return res.status(404).send({
        message: "Not token.",
      });
    }

    const response = await tokenService.decode(token);

    if (
      response.rol == "admin" ||
      response.rol == "vendedor" ||
      response.rol == "encargado"
    ) {
      next();
    } else {
      return res.status(403).json({ message: "No autorizado." });
    }
  },

  verifyAdmin: async (req, res, next) => {
    let token = req.headers.token;

    if (!token) {
      return res.status(404).send({
        message: "Not token.",
      });
    }

    const response = await tokenService.decode(token);

    if (response.rol == "admin") {
      next();
    } else {
      return res.status(403).json({ message: "No autorizado." });
    }
  },

  verifyEncargado: async (req, res, next) => {
    let token = req.headers.token;

    if (!token) {
      return res.status(404).send({
        message: "Not token.",
      });
    }

    const response = await tokenService.decode(token);

    if (response.rol == "admin" || response.rol == "encargado") {
      next();
    } else {
      return res.status(403).json({ message: "No autorizado." });
    }
  },

  verifyVendedor: async (req, res, next) => {
    let token = req.headers.token;

    if (!token) {
      return res.status(404).send({
        message: "Not token.",
      });
    }

    const response = await tokenService.decode(token);

    if (response.rol == "admin" || response.rol == "vendedor") {
      next();
    } else {
      return res.status(403).json({ message: "No autorizado." });
    }
  },
};
