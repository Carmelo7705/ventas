const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const bodyParser = require('body-parser');
const path = require('path');

const app = express();
app.set('port', process.env.PORT || 3000);

//Rutas
const routes = require('./routes/api');

//database
require('./database/db');

app.use(cors());
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use(express.static(path.join(__dirname, 'public')));

//prefix routes
app.use('/', routes);

app.listen(app.get('port'), () => {
	console.log(`Servidor listo en puerto ${app.get('port')}`);
});