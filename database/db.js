const mongoose = require('mongoose');

const { database } = require('./config');

mongoose.connect(database.URI, {
  useCreateIndex: true,
  useNewUrlParser: true
})
  .then(db => console.log(`DB conectada`))
  .catch(err => console.log(err));