const express = require('express');
const router  = express.Router();

const auth = require('../middlewares/auth');

const { 
	CategoryController, 
	ArticlesController, 
	UsersController, 
	PersonsController,
	IngressController,
	SalesController } = require('../controllers');


/** Modulo Categorías */
router.post('/category', auth.verifyEncargado, CategoryController.add);
router.get('/category/:id', auth.verifyEncargado, CategoryController.query);
router.get('/category', auth.verifyEncargado, CategoryController.list);
router.put('/category/update/:id', auth.verifyEncargado, CategoryController.update);
router.delete('/category/:id', auth.verifyEncargado, CategoryController.remove);
router.put('/category/activate/:id', auth.verifyEncargado, CategoryController.activate);
router.put('/category/desactivate/:id', auth.verifyEncargado, CategoryController.desactivate);

/** Modulo Articulos */
router.post('/article', auth.verifyEncargado, ArticlesController.add);
router.get('/article/:id', auth.verifyEncargado, ArticlesController.query);
router.get('/article', auth.verifyEncargado, ArticlesController.list);
router.put('/article/update/:id', auth.verifyEncargado, ArticlesController.update);
router.delete('/article/:id', auth.verifyEncargado, ArticlesController.remove);
router.put('/article/activate/:id', auth.verifyEncargado, ArticlesController.activate);
router.put('/article/desactivate/:id', auth.verifyEncargado, ArticlesController.desactivate);
router.post('/article/queryByCode', auth.verifyEncargado, ArticlesController.queryByCode);


/** Modulo Usuarios */
router.post('/users', auth.verifyAdmin, UsersController.add);
router.get('/users/:id',auth.verifyAdmin, UsersController.query);
router.get('/users', auth.verifyAdmin, UsersController.list);
router.put('/users/update/:id', auth.verifyAdmin, UsersController.update);
router.delete('/users/:id', auth.verifyAdmin, UsersController.remove);
router.put('/users/activate/:id', auth.verifyAdmin, UsersController.activate);
router.put('/users/desactivate/:id', auth.verifyAdmin, UsersController.desactivate);
router.post('/users/signin', UsersController.signin);

/** Modulo Person */
router.post('/persons', auth.userVerify, PersonsController.add);
router.get('/persons/:id',auth.userVerify, PersonsController.query);
router.get('/persons', auth.userVerify, PersonsController.list);
router.get('/persons-clients', auth.userVerify, PersonsController.listClients);
router.get('/persons-proveedores', auth.userVerify, PersonsController.listProveedores);
router.put('/persons/update/:id', auth.userVerify, PersonsController.update);
router.delete('/persons/:id', auth.userVerify, PersonsController.remove);
router.put('/persons/activate/:id', auth.userVerify, PersonsController.activate);
router.put('/persons/desactivate/:id', auth.userVerify, PersonsController.desactivate);

/** Modulo Ingresos */
router.post('/ingress', auth.verifyEncargado, IngressController.add);
router.get('/ingress/:id', auth.verifyEncargado, IngressController.query);
router.get('/ingress', auth.verifyEncargado, IngressController.list);
router.get('/ingress/total-ingress', auth.userVerify, IngressController.totalIngressForYear);
router.put('/ingress/activate/:id', auth.verifyEncargado, IngressController.activate);
router.put('/ingress/desactivate/:id', auth.verifyEncargado, IngressController.desactivate);
router.get('/ingress/query-dates', auth.userVerify, IngressController.query_dates);

/** Modulo Ventas */
router.post('/sales', auth.verifyVendedor, SalesController.add);
router.get('/sales/:id', auth.verifyVendedor, SalesController.query);
router.get('/sales', auth.verifyVendedor, SalesController.list);
router.get('/total_sales', auth.userVerify, SalesController.totalSalesForYear);
router.put('/sales/activate/:id', auth.verifyVendedor, SalesController.activate);
router.put('/sales/desactivate/:id', auth.verifyVendedor, SalesController.desactivate);
router.get('/sales/query-dates', auth.userVerify, SalesController.query_dates);

module.exports = router;