const { Ingress, Article } = require('../models');

async function stockIncrease (article_id, qty) {
	let { stock } = await Article.findOne({_id: article_id});

	let newStock = parseInt(stock) + parseInt(qty);
	const data = await Article.findByIdAndUpdate({_id: article_id}, { stock: newStock });
}

async function stockDecrease (article_id, qty) {
	let { stock } = await Article.findOne({_id: article_id});

	let newStock = parseInt(stock) - parseInt(qty);
	const data = await Article.findByIdAndUpdate({_id: article_id}, { stock: newStock });
}

module.exports = {

	add: async (req, res, next) => {
		try {
			const ingress = await Ingress.create(req.body);
			//Actualizar stock
			let det = req.body.details;
			det.map( result => {
				stockIncrease(result._id, result.qty);
			});

			res.status(200).json(ingress);

		} catch(e) {
			res.status(500).send({
				message: 'Ocurrió un error.'
			});
			next(e);
		}
	},

	query: async (req, res, next) => {
		try {
			const ingress = await Ingress.findOne({_id: req.params.id})
			.populate('user_id', {nombre: 1})
			.populate('person_id', {nombre: 1});

			if(!ingress) {
				res.status(404).send({
					message: 'El registro no existe.'
				});
				return;
			}

			res.status(200).json(ingress);

		} catch(e) {
			res.status(500).send({
				message: 'Ocurrió un error.'
			});
			next(e);
		}
	},

	list: async (req, res, next) => {
		try {
			let val = req.query.val; //req.query = ?val=loqsea

			const ingress = await Ingress.find({
				$or:[
					{'voucher_number': new RegExp(val, 'i')},
					{'voucher_serie': new RegExp(val, 'i')}
				]})
				.populate('user_id', {name: 1})
				.populate('person_id', {name: 1})
				.sort({'createdAt': -1});

			res.status(200).json(ingress);

		} catch(e) {
			res.status(500).send({
				message: 'Ocurrió un error.'
			});
			next(e);
		}
	},

	/*update: async (req, res, next) => {
		try {
			const category = await Category.findByIdAndUpdate({
				_id: req.params.id
			},
			{
				name: req.body.name,
				description: req.body.description,
			}
			);

			res.status(200).json(category);

		} catch(e) {
			res.status(500).send({
				message: 'Ocurrió un error.'
			});
			next(e);
		}
	},

	remove: async (req, res, next) => {
		try {
			const category = await Category.findByIdAndDelete({_id:req.params.id});

			res.status(200).json(category);

		} catch(e) {
			res.status(500).send({
				message: 'Ocurrió un error.'
			});
			next(e);
		}
	},*/

	activate: async (req, res, next) => {
		try {

			const ingress = await Ingress.findByIdAndUpdate({_id: req.params.id}, {status: 1});
			//Actualizar stock
			let det = ingress.details;
			det.map( result => {
				stockIncrease(result._id, result.qty);
			});

			res.status(200).json(ingress);

		} catch(e) {
			res.status(500).send({
				message: 'Ocurrió un error.'
			});
			next(e);
		}
	},

	desactivate: async (req, res, next) => {
		try {

			const ingress = await Ingress.findByIdAndUpdate({_id: req.params.id}, {status: 0});
			//Actualizar stock
			let det = ingress.details;
			det.map( result => {
				stockDecrease(result._id, result.qty);
			});

			res.status(200).json(ingress);
			res.status(200).json(ingress);

		} catch(e) {
			res.status(500).send({
				message: 'Ocurrió un error.'
			});
			next(e);
		}
	},

	totalIngressForYear : async (req, res, next) => {
		try {

			const ingress = await Ingress.aggregate(
				[
					{
						$group: {
							ym: { //ym = Year and month
								month: { $month: '$createdAt' },
								year: { $year: '$createdAt' }
							},
							total: { $sum: '$total' },
							number_sales:{ $sum: 1 } 
						}
					},
					{
						$sort: {
							"ym.year": -1, "ym.month": -1 //Ordename esto primero por el año de manera desdencende, y luego por el mes, igual.
						}
					}
				]
			).limit(12);

			res.status(200).json(ingress);

		} catch(e) {
			res.status(500).send({
				message: 'Ocurrió un error.'
			});
			next(e);
		}
	},

	query_dates: async (req, res, next) => {
		try {

			const ingress = await Ingress.find({
				"createdAt": {'$gte': req.body.start, '$lt': req.body.end}
			})
			.populate('user_id', {nombre: 1})
			.populate('person_id', {nombre: 1})
			.sort({'createdAt': -1});

			res.status(200).json(ingress);

		} catch(e) {
			res.status(500).send({
				message: 'Ocurrió un error.'
			});
			next(e);
		}
	},


}
