const { Person } = require('../models');

module.exports = {

	add: async (req, res, next) => {
		try {

			const person = await Person.create(req.body);
			res.status(200).json(person);

		} catch(e) {
			res.status(500).send({
				message: 'Ocurrió un error.'
			});
			next(e);
		}
	},

	query: async (req, res, next) => {
		try {
			const person = await Person.findOne({_id: req.params.id});
			if(!person) {
				res.status(404).send({
					message: 'El registro no existe.'
				});
				return;
			}

			res.status(200).json(person);



		} catch(e) {
			res.status(500).send({
				message: 'Ocurrió un error.'
			});
			next(e);
		}
	},

	list: async (req, res, next) => {
		try {
			let val = req.query.val; //req.query = ?val=loqsea

			const person = await Person.find({
				$or:[
					{'name': new RegExp(val, 'i')},
					{'email': new RegExp(val, 'i')}
				]})
				.sort({'createdAt': -1});

			res.status(200).json(person);

		} catch(e) {
			res.status(500).send({
				message: 'Ocurrió un error.'
			});
			next(e);
		}
	},

	listClients: async (req, res, next) => {
		try {
			let val = req.query.val; //req.query = ?val=loqsea

			const person = await Person.find({
				$or:[
					{'name': new RegExp(val, 'i')},
					{'email': new RegExp(val, 'i')}
				],
					person_type: 'Cliente' //Un segundo where donde sean igual a clientes
				})
				.sort({'createdAt': -1});

			res.status(200).json(person);

		} catch(e) {
			res.status(500).send({
				message: 'Ocurrió un error.'
			});
			next(e);
		}
	},

	listProveedores: async (req, res, next) => {
		try {
			let val = req.query.val; //req.query = ?val=loqsea

			const person = await Person.find({
				$or:[
					{'name': new RegExp(val, 'i')},
					{'email': new RegExp(val, 'i')}
				],
					person_type: 'Proveedor' //Un segundo where donde sean igual a proveedor
				})
				.sort({'createdAt': -1});

			res.status(200).json(person);

		} catch(e) {
			res.status(500).send({
				message: 'Ocurrió un error.'
			});
			next(e);
		}
	},


	update: async (req, res, next) => {
		try {
			let pass = req.body.pasword;
			
			const person = await Person.findByIdAndUpdate({
				_id: req.params.id
			},
			{	person_type: req.body.person_type,
				name: req.body.name,
				document_type: req.body.document_type,
				document_number: req.body.document_number,
				address: req.body.address,
				phone: req.body.phone,
				email: req.body.email
			}
			);

			res.status(200).json(person);

		} catch(e) {
			res.status(500).send({
				message: 'Ocurrió un error.'
			});
			next(e);
		}
	},

	remove: async (req, res, next) => {
		try {
			const person = await Person.findByIdAndDelete({_id:req.params.id});

			res.status(200).json(person);

		} catch(e) {
			res.status(500).send({
				message: 'Ocurrió un error.'
			});
			next(e);
		}
	},

	activate: async (req, res, next) => {
		try {

			const person = await Person.findByIdAndUpdate({_id: req.params.id}, {status: 1});
			res.status(200).json(person);

		} catch(e) {
			res.status(500).send({
				message: 'Ocurrió un error.'
			});
			next(e);
		}
	},

	desactivate: async (req, res, next) => {
		try {

			const person = await Person.findByIdAndUpdate({_id: req.params.id}, {status: 0});
			res.status(200).json(person);

		} catch(e) {
			res.status(500).send({
				message: 'Ocurrió un error.'
			});
			next(e);
		}
	},

}
