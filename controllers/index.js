const CategoryController = require('./CategoryController');
const ArticlesController = require('./ArticlesController');
const UsersController = require('./UsersController');
const PersonsController = require('./PersonsController');
const IngressController = require('./IngressController');
const SalesController = require('./SalesController');

module.exports = {
	CategoryController,
	ArticlesController,
	UsersController,
	PersonsController,
	IngressController,
	SalesController
}