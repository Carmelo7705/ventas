const { User } = require('../models');
const bcrypt = require('bcryptjs');

const tokenFile = require('../services/token');

module.exports = {

	add: async (req, res, next) => {
		try {
			req.body.password = await bcrypt.hash(req.body.password, 10);

			const user = await User.create(req.body);
			res.status(200).json(user);

		} catch(e) {
			res.status(500).send({
				message: 'Ocurrió un error.'
			});
			next(e);
		}
	},

	query: async (req, res, next) => {
		try {
			const user = await User.findOne({_id: req.params.id});
			if(!user) {
				res.status(404).send({
					message: 'El registro no existe.'
				});
				return;
			}

			res.status(200).json(user);



		} catch(e) {
			res.status(500).send({
				message: 'Ocurrió un error.'
			});
			next(e);
		}
	},

	list: async (req, res, next) => {
		try {
			let val = req.query.val; //req.query = ?val=loqsea

			const user = await User.find({
				$or:[
					{'name': new RegExp(val, 'i')},
					{'email': new RegExp(val, 'i')}
				]})
				.sort({'createdAt': -1});

			res.status(200).json(user);

		} catch(e) {
			res.status(500).send({
				message: 'Ocurrió un error.'
			});
			next(e);
		}
	},

	update: async (req, res, next) => {
		try {;
			
			const verify = await User.findOne({_id: req.params.id}); 

			let pass = req.body.password == '' 
				? verify.password : 
					await bcrypt.hash(req.body.password, 10)

			const user = await User.findByIdAndUpdate({
				_id: req.params.id
			},
			{	rol: req.body.rol,
				name: req.body.name,
				document_type: req.body.document_type,
				document_number: req.body.document_number,
				address: req.body.address,
				phone: req.body.phone,
				email: req.body.email,
				password: pass,
			}
			);

			res.status(200).json(user);

		} catch(e) {
			res.status(500).send({
				message: 'Ocurrió un error.'
			});
			next(e);
		}
	},

	remove: async (req, res, next) => {
		try {
			const user = await User.findByIdAndDelete({_id:req.params.id});

			res.status(200).json(user);

		} catch(e) {
			res.status(500).send({
				message: 'Ocurrió un error.'
			});
			next(e);
		}
	},

	activate: async (req, res, next) => {
		try {

			const user = await User.findByIdAndUpdate({_id: req.params.id});
			res.status(200).json(user);

		} catch(e) {
			res.status(500).send({
				message: 'Ocurrió un error.'
			});
			next(e);
		}
	},

	desactivate: async (req, res, next) => {
		try {

			const user = await User.findByIdAndUpdate({_id: req.params.id}, {status: 0});
			res.status(200).json(user);

		} catch(e) {
			res.status(500).send({
				message: 'Ocurrió un error.'
			});
			next(e);
		}
	},

	signin: async (req, res, next) => {
		try {

			let user = await User.findOne({email: req.body.email});

			if(!user) {
				return res.status(403).send({
					message: 'Email inexistente.'
				});
			}

			let match = await bcrypt.compare(req.body.password, user.password);

			if(!match) {
				return res.status(403).send({
					message: 'La contraseña es incorrecta.'
				});
			}

			let token = await tokenFile.encode(user._id, user.rol, user.email);
			return res.status(200).json({data: user, token: token, success: true});

		} catch(e) {
			res.status(500).send({
				message: 'Ocurrió un error.'
			});
			next(e);
		}
	}


}
