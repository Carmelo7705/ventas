const { Article } = require('../models');

module.exports = {

	add: async (req, res, next) => {
		try {
			const article = await Article.create(req.body);
			res.status(200).json(article);

		} catch(e) {

			/*A diferencia de campos como required o min, el unique de mongodb no se le puede agregar un 
			mensaje de error particular. Se le debe manejar de esta forma con if. Eso sí, el ternario queda
			mucho mejor organizado y legible.*/

			let msg = e.name == 'MongoError' && e.code == 11000 ? 
				'El valor ya existe en la base de datos.' : e;

			res.status(500).send({
				message: 'Ocurrió un error.',
				msg: msg
			});
			next(e);
		}
	},

	query: async (req, res, next) => {
		try {
			const article = await Article.findOne({_id: req.params.id})
				.populate('category', {nombre:1, description: 1}) //De la referencia que existe con el modelo Category, me vas a mostrar su nombre, y su descripción.;
			if(!article) {
				res.status(404).send({
					message: 'El registro no existe.'
				});
				return;
			}

			res.status(200).json(article);



		} catch(e) {
			res.status(500).send({
				message: 'Ocurrió un error.'
			});
			next(e);
		}
	},

	list: async (req, res, next) => {
		try {
			let val = req.query.val; //req.query = ?val=loqsea

			const article = await Article.find({
				$or:[
					{'name': new RegExp(val, 'i')},
					{'description': new RegExp(val, 'i')}
				]})
				.populate('category', {name:1})
				.sort({'createdAt': -1});

			res.status(200).json(article);

		} catch(e) {
			res.status(500).send({
				message: 'Ocurrió un error.'
			});
			next(e);
		}
	},

	update: async (req, res, next) => {
		try {
			const article = await Article.findByIdAndUpdate({
				_id: req.params.id
			},
			{
				category: req.body.category,
				code: req.body.code,
				name: req.body.name,
				description: req.body.description,
				price: req.body.price,
				stock: req.body.stock,
			}
			);

			res.status(200).json(article);

		} catch(e) {
			res.status(500).send({
				message: 'Ocurrió un error.',
				msg: e
			});
			next(e);
		}
	},

	remove: async (req, res, next) => {
		try {
			const article = await Article.findByIdAndDelete({_id:req.params.id});

			res.status(200).json(article);

		} catch(e) {
			res.status(500).send({
				message: 'Ocurrió un error.'
			});
			next(e);
		}
	},

	activate: async (req, res, next) => {
		try {

			const article = await Article.findByIdAndUpdate({_id: req.params.id}, {status: 1});
			res.status(200).json(article);

		} catch(e) {
			res.status(500).send({
				message: 'Ocurrió un error.'
			});
			next(e);
		}
	},

	desactivate: async (req, res, next) => {
		try {

			const article = await Article.findByIdAndUpdate({_id: req.params.id}, {status: 0});
			res.status(200).json(article);

		} catch(e) {
			res.status(500).send({
				message: 'Ocurrió un error.'
			});
			next(e);
		}
	},

	queryByCode : async (req, res, next) => {
		try {
			const article = await Article.findOne({code: req.body.code})
				.populate('category', {name:1});

			if(!article) {
				res.status(403).json({message: "No existe el registro para este código."});
				return;
			}

			res.status(200).json(article);

		} catch(e) {
			res.status(500).send({
				message: 'Ocurrió un error.'
			});
			next(e);
		}
	}


}
