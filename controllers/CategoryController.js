const { Category } = require('../models');

module.exports = {

	add: async (req, res, next) => {
		try {
			console.log(req.body);
			const category = await Category.create(req.body);
			res.status(200).json(category);

		} catch(e) {
			res.status(500).send({
				message: 'Ocurrió un error.'
			});
			next(e);
		}
	},

	query: async (req, res, next) => {
		try {
			const category = await Category.findOne({_id: req.params.id});
			if(!category) {
				res.status(404).send({
					message: 'El registro no existe.'
				});
				return;
			}

			res.status(200).json(category);



		} catch(e) {
			res.status(500).send({
				message: 'Ocurrió un error.'
			});
			next(e);
		}
	},

	list: async (req, res, next) => {
		try {
			let val = req.query.val; //req.query = ?val=loqsea

			const category = await Category.find({
				$or:[
					{'name': new RegExp(val, 'i')},
					{'description': new RegExp(val, 'i')}
				]})
				.populate('articles')
				.sort({'createdAt': -1});

			res.status(200).json(category);

		} catch(e) {
			res.status(500).send({
				message: 'Ocurrió un error.'
			});
			next(e);
		}
	},

	update: async (req, res, next) => {
		try {
			const category = await Category.findByIdAndUpdate({
				_id: req.params.id
			},
			{
				name: req.body.name,
				description: req.body.description,
			}
			);

			res.status(200).json(category);

		} catch(e) {
			res.status(500).send({
				message: 'Ocurrió un error.'
			});
			next(e);
		}
	},

	remove: async (req, res, next) => {
		try {
			const category = await Category.findByIdAndDelete({_id:req.params.id});

			res.status(200).json(category);

		} catch(e) {
			res.status(500).send({
				message: 'Ocurrió un error.'
			});
			next(e);
		}
	},

	activate: async (req, res, next) => {
		try {

			const category = await Category.findByIdAndUpdate({_id: req.params.id}, {status: 1});
			res.status(200).json(category);

		} catch(e) {
			res.status(500).send({
				message: 'Ocurrió un error.'
			});
			next(e);
		}
	},

	desactivate: async (req, res, next) => {
		try {

			const category = await Category.findByIdAndUpdate({_id: req.params.id}, {status: 0});
			res.status(200).json(category);

		} catch(e) {
			res.status(500).send({
				message: 'Ocurrió un error.'
			});
			next(e);
		}
	}


}
