const { Sale, Article } = require('../models');

async function stockIncrease (article_id, qty) {
	let { stock } = await Article.findOne({_id: article_id});

	let newStock = parseInt(stock) + parseInt(qty);
	const data = await Article.findByIdAndUpdate({_id: article_id}, { stock: newStock });
}

async function stockDecrease (article_id, qty) {
	let { stock } = await Article.findOne({_id: article_id});

	let newStock = parseInt(stock) - parseInt(qty);
	const data = await Article.findByIdAndUpdate({_id: article_id}, { stock: newStock });
}

module.exports = {

	add: async (req, res, next) => {
		try {
			const sales = await Sale.create(req.body);
			//Actualizar stock
			let det = req.body.details;
			det.map( result => {
				stockDecrease(result._id, result.qty);
			});

			res.status(200).json(sales);

		} catch(e) {
			res.status(500).send({
				message: 'Ocurrió un error.'
			});
			next(e);
		}
	},

	query: async (req, res, next) => {
		try {
			const sales = await Sale.findOne({_id: req.params.id})
			.populate('user_id', {nombre: 1})
			.populate('person_id', {nombre: 1});

			if(!sales) {
				res.status(404).send({
					message: 'El registro no existe.'
				});
				return;
			}

			res.status(200).json(sales);



		} catch(e) {
			res.status(500).send({
				message: 'Ocurrió un error.'
			});
			next(e);
		}
	},

	list: async (req, res, next) => {
		try {
			let val = req.query.val; //req.query = ?val=loqsea

			const sales = await Sale.find({
				$or:[
					{'voucher_number': new RegExp(val, 'i')},
					{'voucher_serie': new RegExp(val, 'i')}
				]})
				.populate('user_id', {name: 1})
				.populate('person_id')
				.sort({'createdAt': -1});

			res.status(200).json(sales);

		} catch(e) {
			res.status(500).send({
				message: 'Ocurrió un error.'
			});
			next(e);
		}
	},

	/*update: async (req, res, next) => {
		try {
			const category = await Category.findByIdAndUpdate({
				_id: req.params.id
			},
			{
				name: req.body.name,
				description: req.body.description,
			}
			);

			res.status(200).json(category);

		} catch(e) {
			res.status(500).send({
				message: 'Ocurrió un error.'
			});
			next(e);
		}
	},

	remove: async (req, res, next) => {
		try {
			const category = await Category.findByIdAndDelete({_id:req.params.id});

			res.status(200).json(category);

		} catch(e) {
			res.status(500).send({
				message: 'Ocurrió un error.'
			});
			next(e);
		}
	},*/

	activate: async (req, res, next) => {
		try {

			const sale = await Sale.findByIdAndUpdate({_id: req.params.id}, {status: 1});
			//Actualizar stock
			let det = sale.details;
			det.map( result => {
				stockDecrease(result._id, result.qty);
			});

			res.status(200).json(sale);

		} catch(e) {
			res.status(500).send({
				message: 'Ocurrió un error.'
			});
			next(e);
		}
	},

	desactivate: async (req, res, next) => {
		try {

			const sales = await Sale.findByIdAndUpdate({_id: req.params.id}, {status: 0});
			//Actualizar stock
			let det = sales.details;
			det.map( result => {
				stockIncrease(result._id, result.qty);
			});

			res.status(200).json(sales);

		} catch(e) {
			res.status(500).send({
				message: 'Ocurrió un error.'
			});
			next(e);
		}
	},

	totalSalesForYear : async (req, res, next) => {
		try {

			const sale = await Sale.aggregate(
				[
					{
						$group: {
							_id: { //_id = Year and month
								month: { $month: "$createdat" },
								year: { $year: "$createdat" }
							},
							total: { $sum: "$total" },
							number_sales:{ $sum: 1 } 
						}
					},
					{
						$sort: {
							"_id.year": -1,"_id.month": -1 //Ordename esto primero por el año de manera desdencende, y luego por el mes, igual.
						}
					}
				]
			).limit(12);

			res.status(200).json(sale);

		} catch(e) {
			res.status(500).send({
				message: 'Ocurrió un error.'
			});
			next(e);
		}
	},

	query_dates: async (req, res, next) => {
		try {

			const sales = await Sale.find({
				"createdAt": {'$gte': req.body.start, '$lt': req.body.end}
			})
			.populate('user_id', {nombre: 1})
			.populate('person_id', {nombre: 1})
			.sort({'createdAt': -1});

			res.status(200).json(sales);

		} catch(e) {
			res.status(500).send({
				message: 'Ocurrió un error.'
			});
			next(e);
		}
	},


}
