const mongoose = require('mongoose');
const { Schema, ObjectId } = mongoose;

const ArticleSchema = new Schema({
	category: { type: ObjectId, ref: 'Category'},
	code: { type: String, maxlength: 64, required: [true, 'Codigo obligatorio.'], unique:true },
	name: { type: String, maxlength: 50 },
	description: { type: String, maxlength: 255 },
	price: { type: Number, required: true },
	stock: { type: Number, required: true },
	status: { type: Number, default: 1 },
	createdAt: { type: Date, default: Date.now }
});

module.exports = mongoose.model('Article', ArticleSchema);