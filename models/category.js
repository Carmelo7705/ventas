const mongoose = require('mongoose');
const { Schema, ObjectId } = mongoose;

CategorySchema = new Schema({
	name: { type: String, maxLength: 50, unique: true, required: true },
	description: { type: String, maxLength: 255 },
	status: { type: Number, default: 1 },
	createdAt: { type: Date, default: Date.now }
});

CategorySchema.virtual('articles', {
  ref: 'Article', // The model to use
  localField: '_id', // Find people where `localField`
  foreignField: 'category', // is equal to `foreignField`
  //justOne: false, //justOne false es uno a muchos, true es 1 a 1.
  //options: { sort: { name: -1 }, limit: 5 } // Query options, see http://bit.ly/mongoose-query-options
});


CategorySchema.set('toObject', { virtuals: true });
CategorySchema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('Category', CategorySchema);