const Category = require('./category');
const Article = require('./article');
const User = require('./user');
const Person = require('./person');
const Ingress = require('./ingress');
const Sale = require('./sale');

module.exports = {
	Category,
	Article,
	User,
	Person,
	Ingress,
	Sale
}