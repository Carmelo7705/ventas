const mongoose = require('mongoose');
const { Schema, ObjectId } = mongoose;

const IngressSchema = new Schema({
	user_id: { type: ObjectId, ref: 'User', required: true },
	person_id: { type: ObjectId, ref: 'Person', required: true },
	voucher_type: { type: String, maxlength: 20, required: true },
	voucher_serie: { type: String, maxlength: 7 },
	voucher_number: { type: String, maxlength: 10, required: true },
	tax: { type: Number, required: true },
	total: { type: Number, required: true },
	details: [{
		_id: {
			type: String,
			required: true
		},
		article: {
			type: String,
			required: true
		},
		qty: {
			type: Number,
			required: true
		},
		price: {
			type: Number,
			required: true
		}
	}],
	status: { type: Number, default: 1 },
	createdat: { type: Date, default: Date.now },
});

module.exports = mongoose.model('Ingress', IngressSchema);