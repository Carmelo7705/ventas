const mongoose = require('mongoose');
const { Schema } = mongoose;

const UserSchema = new Schema({
	rol: { type: String, maxLength: 30, required: true },
	name: { type: String, maxLength: 50, required: true },
	document_type: { type: String, maxLength: 20 },
	document_number: { type: String, maxLength: 30 },
	address: { type: String, maxLength: 70 },
	phone: { type: String, maxLength: 20 },
	email: { type: String, maxLength: 50, unique:  true, required: true },
	password: { type: String, maxLength: 64, required: true }
});

module.exports = mongoose.model('User', UserSchema);