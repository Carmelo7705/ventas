//Modelo tanto para clientes como proveedores.
const mongoose = require('mongoose');
const { Schema } = mongoose;

const PersonSchema = new Schema({
	person_type: { type: String, maxLength: 20, required: true }, //Es cliente o proveedor
	name: { type: String, maxLength: 50, required: true },
	document_type: { type: String, maxLength: 20 },
	document_number: { type: String, maxLength: 30 },
	address: { type: String, maxLength: 70 },
	phone: { type: String, maxLength: 20 },
	email: { type: String, maxLength: 50, unique: true },
	status: { type: Number, default: 1},
	createdAt: { type: Date, default: Date.now },
});

module.exports = mongoose.model('Person', PersonSchema);