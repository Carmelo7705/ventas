import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store';

import Home from '../views/Home.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/login',
    name: 'login',
    component: () => import('../views/Auth/Login'),
    meta: {
      free: true
    }
  },
  {
    path: '/',
    name: 'Home',
    component: Home,
    meta: {
      admin: true,
      vendedor: true,
      encargado: true
    }
  },
  {
    path: '/category',
    name: 'Category',
    component: () => import('../views/Categories/Category'),
    meta: {
      admin: true,
      encargado: true
    }
  },
  {
    path: '/article',
    name: 'Article',
    component: () => import('../views/Articles/Article'),
    meta: {
      admin: true,
      encargado: true
    }
  },
  {
    path: '/clients',
    name: 'Client',
    component: () => import('../views/Clients/Client'),
    meta: {
      admin: true,
      vendedor: true
    }
  },
  {
    path: '/providers',
    name: 'Proveedor',
    component: () => import('../views/Providers/Provider'),
    meta: {
      admin: true,
      encargado: true
    }
  },
  {
    path: '/ingress',
    name: 'Ingress',
    component: () => import('../views/Ingress/Ingress'),
    meta: {
      admin: true,
      encargado: true
    }
  },
  {
    path: '/sales',
    name: 'Sale',
    component: () => import('../views/Sales/Sale'),
    meta: {
      admin: true,
      vendedor: true
    }
  },
  {
    path: '/users',
    name: 'User',
    component: () => import('../views/Users/User'),
    meta: {
      admin: true
    }
  },

  
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  if(to.matched.some(record => record.meta.free)) {
    next();
  } else if(store.getters.token && store.getters.user.rol == "admin") {
    if(to.matched.some(record => record.meta.admin)) {
      next();
    }
  } else if(store.getters.token && store.getters.user.rol == "encargado") {
    if(to.matched.some(record => record.meta.encargado)) {
      next();
    }
  } else if(store.getters.token && store.getters.user.rol == "vendedor") {
    if(to.matched.some(record => record.meta.vendedor)) {
      next();
    }
  } else {
    next({name: "login"});
  }
});

export default router
