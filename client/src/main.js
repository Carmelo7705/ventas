import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';

import moment from 'moment'
moment.locale('es');
Vue.prototype.$moment = moment
window.moment = moment;

import axios from 'axios';

Vue.config.productionTip = false
Vue.prototype.$http = axios; //Para usar axios de manera global con solo llamar this.$http
axios.defaults.baseURL = 'http://localhost:3000/';

import VueAlertify from 'vue-alertify';
Vue.use(VueAlertify, {
  notifier: {
    // auto-dismiss wait time (in seconds)
    delay: 5,
    // default position
    position: 'top-right',

  },
});


new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
