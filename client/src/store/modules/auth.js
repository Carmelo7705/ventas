import decode from 'jwt-decode';
import router from '../../router';

export default {
	state: {
		token: null,
		user: null
  },
  mutations: {
  	SET_TOKEN(state, token) {
  		state.token = token;
  	},

  	SET_USER(state, user) {
  		state.user = user;
  	}
  },
  getters: {
  	token: state => state.token,
  	user: state => state.user,
  },
  actions: {
  	token({commit}, token) {
  		commit("SET_TOKEN", token);
			commit("SET_USER", decode(token));
  		localStorage.setItem("token", token);
  	},

  	autologin({commit}) {
  		let token = localStorage.getItem("token");
  		if(token) {
  			commit("SET_TOKEN", token);
  			commit("SET_USER", decode(token));
		  }

  		router.push({name: "Home"});
  	},

  	logout({commit}) {
  		commit("SET_TOKEN", null);
  		commit("SET_USER", null);
  		localStorage.removeItem("token");

  		router.push({name: "login"});
  	}
  },
}