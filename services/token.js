const jwt = require('jsonwebtoken');
const { User } = require('../models');

async function checkToken(token) { //Para validar el token, si ha expirado o no.
	let __id = null;

	try {
		const {_id} = await jwt.decode(token);
		__id = _id;
	} catch(e) {
		return false; //Si ocurriera un error, eso quiere que el token, no es que ha expirado o algo, sino que es inválido.
	}

	const user = await User.findOne({_id:__id});
	if(!user) { //Si el usuario no existe, retornamos falso. token inexistente o inválido.
		return false;
	}

	const newToken = jwt.sign({_id:__id, rol: user.rol, email: user.email}, '123456',{expiresIn: '1d'}); //Si el usuario existe, le renovamos el token de nuevo
	return {newToken,rol:user.rol};
}

module.exports = {
	encode: async (_id, rol, email) => {
		const token = jwt.sign({_id: _id, rol: rol, email: email},'123456',{expiresIn: '1d'});
		return token;
	},

	decode: async (token) => {
		try {
			const {_id} = await jwt.verify(token, '123456');
			const user = await User.findOne({_id});

			if(!user) {
				return false;
			}

			return user;

		} catch(e) {
			const newToken = await checkToken(token);
			return newToken;
		}
	}
}